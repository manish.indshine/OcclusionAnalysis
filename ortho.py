#########################################################
# TERMINOLOGIES
#   Image origin =  top left corner
#   Row =           positive when go down
#   Col =           positive when go right side
#   Pixel_x =       number of col
#   Pixel_y =       number of row
#   Widthofpixel  = number of col
#   Heightofpixel = number of row
#   Easting =       number of col
#   Northing =     -number of row

#########################################################
def latlong2utm(lat,long):
    coord = utm.from_latlon(lat, long);
    easting =  coord[0];
    northing = coord[1];
    zone = coord[2];
    hemi = coord[3];
    return easting,northing,zone,hemi

def rowcol2coord(row,col,xOrigin,yOrigin,pixelWidth,pixelHeight):
    easting = xOrigin + col*pixelWidth;
    northing = yOrigin - row*pixelHeight;
    return easting,northing    

def coord2rowcol(x,y,xOrigin,yOrigin,pixelWidth,pixelHeight):
    col = np.int(((x - xOrigin) / pixelWidth)); # X increases as we go right
    row = np.int(((yOrigin - y) / pixelHeight));   # Y decreases as we go down
    return row,col
    
# For converting projected to geographic coordinate system
def xy2latlong(easting,northing,zone,hemi):
    lat,long = utm.to_latlon( easting,northing,zone,hemi);
    return lat,long
    
def floatrange(start, stop, step):
    while start < (stop + step):
        yield start
        start += step
        
def get_txt_file(filename):
   f = open(filename).read()
   matrix = [item.split() for item in f.split('\n')[:-1]];
   return matrix

def point_vector(xp,yp,zp,xi,yi,zi):
    vector = [xi-xp,yi-yp,zi-zp];
    return np.asarray(vector)
    
def image_bbox(xi,yi,radius):
    rx = xi + radius; # Right side x
    lx = xi - radius; # Left side x
    uy = yi + radius; # Upper y, northing increases as we go up
    dy = yi - radius; # lower y, northing increases as we go up
    return [lx,uy,rx,dy];

def cart2polar(x, y):
    r = np.sqrt(x**2 + y**2);
    theta = np.arctan2(y, x);
    return r, theta

def polar2cart(r, theta):
    x = r * np.cos(theta);
    y = r * np.sin(theta);
    return x, y  

def roi(origin_row,origin_col, radius, pixelHeight, pixelWidth):
    cols = int(radius/pixelWidth);
    rows = int(radius/pixelHeight);
    ul = [origin_row - rows,origin_col - cols];
    br = [origin_row + rows,origin_col + cols];
    return np.asarray(ul),np.asarray(br)

def between(x,a,b):
    if x>a and x<b:
        betw = 1;
    else:
        betw = 0;
    return betw
    
def filloutraster(inputlayer,ul,br,baselayer):
    zeros = np.zeros(baselayer.shape[0:2]);
    zeros[ul[0]: ul[0]+ image_dsm_array.shape[0],ul[1]:br[1]] = inputlayer;
    return np.dstack([baselayer,zeros])
    
    
import time 
start_time = time.time()
import gdal
import numpy as np
import utm
import cv2
import matplotlib.pyplot as plt
import Matrix_BackProject as bp

dsm_location = '/home/indshine-2/Downloads/DEMS/Ortho/Khurrampur/DEM_All_GCP.tif';
ortho_location = '/home/indshine-2/Downloads/DEMS/Ortho/Khurrampur/Ortho_same_cellsize.tif';
camera_file = '/home/indshine-2/Downloads/DEMS/Ortho/Khurrampur/Khurrampur_camera.txt';
image_location = '/home/indshine-2/Downloads/DEMS/Ortho/Khurrampur/images/';

camera_data = get_txt_file(camera_file);
total_images = len(camera_data);

driver = gdal.GetDriverByName('GTiff');       
# Opening of rater. This consumed most of the step
dataset = gdal.Open(dsm_location);
ortho_ = gdal.Open(ortho_location);

# Setting noData value to -9999
bands=dataset.GetRasterBand(1);
bands.SetNoDataValue(-9999);

# Getting geo information from Tiff
transform = dataset.GetGeoTransform();

# Getting origin of raster
xOrigin = transform[0];
yOrigin = transform[3];

# Getting pixel width and height
pixelWidth = transform[1];
pixelHeight = -transform[5];

dsm_array = np.float64(np.array(dataset.GetRasterBand(1).ReadAsArray()))

_ortho_array_red = np.float64(np.array(ortho_.GetRasterBand(1).ReadAsArray()));
_ortho_array_green = np.float64(np.array(ortho_.GetRasterBand(2).ReadAsArray()));
_ortho_array_blue = np.float64(np.array(ortho_.GetRasterBand(3).ReadAsArray()));

ortho_array = np.dstack((_ortho_array_red,_ortho_array_green,_ortho_array_blue));

dsm_array[dsm_array==-32767.000]=np.nan;
#visibility_multi = np.zeros(dsm_array.shape);

for i in range(2,total_images):
    
    normal_vector = np.asarray([0,0,-1]);     
    radius = 50;
    lat = float(camera_data[i][2]);
    long = float(camera_data[i][1]);
    image_ele = float(camera_data[i][3]);
    image_name = camera_data[i][0];
    
    im = cv2.imread(image_location + image_name + '.JPG');
    
    image_size_col = im.shape[1];
    image_size_row = im.shape[0];
    no_of_channels = im.shape[2];
    im = np.reshape(im,[4000,6000,no_of_channels]);

#    if image_size_col>image_size_row:
#        im = np.reshape(im,[image_size_row,image_size_col,no_of_channels]);
#    else:
#        im = np.reshape(im,[image_size_col,image_size_row,no_of_channels]);
#        cols_ = image_size_row;
#        rows_ = image_size_col;
#        image_size_col = cols_
#        image_size_row = rows_
    
    
    
    image_easting, image_northing , zone, hemi = latlong2utm(lat,long);
    # Bbox will contain lx,uy,rx,dy (leftx , uppery , rightx , downy)
    bbox = image_bbox(image_easting,image_northing,radius);

#    ul_row,ul_col = coord2rowcol(bbox[0],bbox[1],xOrigin,yOrigin,pixelWidth,pixelHeight);
#    br_row,br_col = coord2rowcol(bbox[2],bbox[3],xOrigin,yOrigin,pixelWidth,pixelHeight);
  
    origin_row,origin_col = coord2rowcol(image_easting,image_northing,xOrigin,yOrigin,pixelWidth,pixelHeight);

    ul,br = roi(origin_row,origin_col, radius, pixelHeight, pixelWidth);
    ul[np.where(ul<0)] = 0;
    br[np.where(br<0)] = 0;
    
#    row = np.asarray(range(ul[0], br[0])); # X will change col
#    col = np.asarray(range(ul[1], br[1])); # and Y will change row
#    easting, northing = rowcol2coord(row,col,xOrigin,yOrigin,pixelWidth,pixelHeight);
    
    easting = np.linspace((image_easting - radius),(image_easting + radius),num = 2*radius/pixelWidth,endpoint=True);
    northing= np.linspace((image_northing - radius),(image_northing + radius),num = 2*radius/pixelHeight,endpoint=True);
    
    easting,northing = np.meshgrid(easting,northing);
    
    rel_easting  = easting  - image_easting;
    rel_northing = northing - image_northing;
        
    image_dsm_array   = dsm_array[ul[0]:br[0],ul[1]:br[1]];
    image_ortho_array = ortho_array[ul[0]:br[0],ul[1]:br[1]];
    
    # Here I am assuming that out of bound can only go from right side.
    # This assumption is correct because at this line ul[np.where(ul<0)] = 0;
    # I have already corrected left side out of bound by clipping it to zero
    # So I we are safe in assuming
    
    rel_easting  = rel_easting[0:image_dsm_array.shape[0],0:image_dsm_array.shape[1]]
    rel_northing = rel_northing[0:image_dsm_array.shape[0],0:image_dsm_array.shape[1]]
    
    easting = easting[0:image_dsm_array.shape[0],0:image_dsm_array.shape[1]];
    northing = northing[0:image_dsm_array.shape[0],0:image_dsm_array.shape[1]];
    
    # This is vec(a).veb(b) Since we have assumed that image is looking vertically
    # downwards normal_vector is [0,0,-1] and point is x,y,z
    
    rel_ele = image_dsm_array - image_ele;
    position_vector = np.asarray([rel_easting,rel_northing,rel_ele]);
    
    # This numerator is vec(a).vec(b) . Here \ is used to move to next line
    numerator = position_vector[0]*normal_vector[0] +\
    position_vector[1]*normal_vector[1]+position_vector[2]*normal_vector[2];
    
    # This div is \a\*\b\ 
    div = np.sqrt(rel_easting**2 + rel_northing**2 + rel_ele**2);
    alpha = np.arccos(numerator/div);
    
    radial,theta = cart2polar(rel_easting,rel_northing);
    visibility = np.zeros(alpha.shape);
    
    #radial_arrage = np.linspace(radial.min(), radial.max(), len(col))
    #theta_arrage = np.linspace(theta.min(), theta.max(), len(row))
    #radial_grid, theta_grid = np.meshgrid(radial_arrage, theta_arrage)
    
    count = 0;
    total = 0;
    for theta_ in floatrange(theta.min(),theta.max(),pixelWidth/radial.max()):
        # At radial 0, it will be visible since point is just below image
        # Calculating alpha of this point so as to compare with other alphas
    
        _origin_col = np.where(abs(rel_easting[0,:]).min()==abs(rel_easting[0,:]))[0][0];
        _origin_row = np.where(abs(rel_northing[:,0]).min()==abs(rel_northing[:,0]))[0][0];
        
        alpha_last_visible = alpha[_origin_row,_origin_col];
        # If images are lying outside of processing area where DSM is not present
        # then assuming image is looking downwards 
        if alpha_last_visible != alpha_last_visible:
            alpha_last_visible = 0;
        visibility_prev = 1; 

        # This should be adaptive as given in the paper
        for radial_ in floatrange(radial.min(),radial.max(),pixelWidth):
            # Sampling x and y row and col number according to R and Theta
            x,y = polar2cart(radial_,theta_);
            # Calculating geo coordinates of this row and col
            x =x + image_easting;
            y =y + image_northing;
            total = total + 1;
            # This row and col is with respect to complete dsm array
            # We have to bring this to image_dsm_array which we have done later
            
            row_,col_ = coord2rowcol(x,y,xOrigin,yOrigin,pixelWidth,pixelHeight);
            
            # Since we are concered with the 50x50 box but not radius of 50, 
            # many points outside this box will be selected too. So these must 
            # be removed.
            
            if row_ < ul[0] or row_ > br[0]:
                count = count +1;
                continue;
            if col_ < ul[1] or col_ > br[1]:
                count = count +1;            
                continue;

            # Bringing row and col wrt dsm_array to row and col wrt to 
            # image_dsm_array
                
            row_ = row_ - ul[0];
            col_ = col_ - ul[1];
            
            if row_ > image_dsm_array.shape[0]-1:
                count = count +1;
                continue;
            if col_ > image_dsm_array.shape[1]-1:
                count = count +1;            
                continue;
                
            alpha_ = alpha[row_,col_];
            
            if alpha_ > alpha_last_visible:
                visibility[row_,col_] = 1;
                alpha_last_visible = alpha_;
                continue;
            else:
                visibility[row_,col_] = 0;
                continue;
    easting  = (rel_easting  + image_easting);
    northing = (rel_northing + image_northing);
    ele      = (rel_ele      + image_ele);
    
#    easting[easting==0]=np.nan;
#    northing[northing==0]=np.nan;
#    ele[ele==0]=np.nan;

    pixel_x,pixel_y = bp.BackProject(easting,northing,ele,camera_file,image_name);
    # Pixel_x contains columns.
    # Pixel_y contains rows.
    
    pixel_x[pixel_x>(6000-1)] = 0;
    pixel_y[np.where(np.greater(pixel_y,4000-1))] = 0;
    
    image= im[pixel_y,pixel_x];
    
#    image[:,:,0] = image[:,:,0]*visibility;
#    image[:,:,1] = image[:,:,1]*visibility;
#    image[:,:,2] = image[:,:,2]*visibility;

    endtime = time.time() - start_time;
    cv2.imwrite('/home/indshine-2/Downloads/DEMS/Ortho/' + str(i) + '_' + camera_data[i][0] + '.jpg' ,image)
    cv2.imwrite('/home/indshine-2/Downloads/DEMS/Ortho/' + str(i) + 'v_' + camera_data[i][0] + '.jpg' ,visibility*255)

    print i , endtime